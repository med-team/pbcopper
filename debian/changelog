pbcopper (2.3.0+dfsg-3) UNRELEASED; urgency=medium

  * Team upload.
  * d/control: simplify architecture specifications by using provisions
    from the architecture-properties package.
  * d/salsi-ci.yml: skip i386 build, add link to salsa-ci docs.

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 07 Mar 2024 11:43:47 +0100

pbcopper (2.3.0+dfsg-2) unstable; urgency=medium

  * Build-Depends: s/libboost1.81-dev/libboost-dev/
    Closes: #1060791

 -- Andreas Tille <tille@debian.org>  Thu, 25 Jan 2024 20:05:13 +0100

pbcopper (2.3.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Bump SOVERSION since upstream bumps it with every new version
  * d/watch: Fix download name
  * Add build support for loong64
    Closes: #1057626
  * Update d/copyright

 -- Andreas Tille <tille@debian.org>  Thu, 07 Dec 2023 14:49:43 +0100

pbcopper (2.2.0+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

  [ Michael R. Crusoe ]
  * d/patches/gcc-13: fix build failures (Closes: #1041127)
  * d/control: use newer boost version for c++20 compat
  * d/patches: mark Debian specific patches as not needing forwarding.
  * d/control: rename library package to match soversion (2.2.0

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 09 Aug 2023 15:48:38 +0200

pbcopper (2.0.0+dfsg-2) unstable; urgency=medium

  * Skip some flaky tests

 -- Andreas Tille <tille@debian.org>  Thu, 24 Feb 2022 18:26:35 +0100

pbcopper (2.0.0+dfsg-1) unstable; urgency=medium

  * Mention CC0-1.0 in copyright
  * New upstream version
  * Follow upstream soname bump

 -- Andreas Tille <tille@debian.org>  Thu, 17 Feb 2022 20:06:42 +0100

pbcopper (1.9.3+dfsg-2) unstable; urgency=medium

  * Fix watch file

 -- Andreas Tille <tille@debian.org>  Thu, 02 Dec 2021 21:08:24 +0100

pbcopper (1.9.3+dfsg-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * Upstream now uses simde, adapt accordingly

  [ Steffen Moeller ]
  * Fix watchfile to detect new versions on github (routine-update)
  * uscan-warning: removing trailing / in d/copyright Files-Excluded

  [ Andreas Tille ]
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * bump SO version to 1.9.3

 -- Andreas Tille <tille@debian.org>  Mon, 27 Sep 2021 17:16:16 +0200

pbcopper (1.8.0+dfsg-2) unstable; urgency=medium

  * Source only upload.
    Closes: 964104

 -- Andreas Tille <tille@debian.org>  Fri, 13 Nov 2020 21:38:40 +0100

pbcopper (1.8.0+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
  * debhelper-compat 13 (routine-update)

  [ Étienne Mollier ]
  * Add boost_optional.patch: fixes build issues with newer boost versions.

 -- Andreas Tille <tille@debian.org>  Thu, 12 Nov 2020 16:12:49 +0100

pbcopper (1.6.0+dfsg-4) unstable; urgency=medium

  * Add alpha to list of architectures
    Closes: #961774
  * libpbcopper1.6.0: Breaks+Replaces: libpbcopper1.3.0 (>= 1.6)
    Closes: #961759

 -- Andreas Tille <tille@debian.org>  Fri, 29 May 2020 13:14:48 +0200

pbcopper (1.6.0+dfsg-3) unstable; urgency=medium

  * ppc64el patch is not needed for libsimde-dev >= 0.0.0.git.20200526

 -- Andreas Tille <tille@debian.org>  Wed, 27 May 2020 12:38:57 +0200

pbcopper (1.6.0+dfsg-2) unstable; urgency=medium

  * Use d-shlibs to make sure SOVERSION is correct
    Closes: #959409
  * d-shlibs requires explicitly Section: libs
  * Restrict architectures to 64bit
    Closes: #960219
  * Fix simde build for ppc64el (thanks to Michael Hudson-Doyle for the patch)
    Closes: #961581

 -- Andreas Tille <tille@debian.org>  Tue, 26 May 2020 13:28:10 +0200

pbcopper (1.6.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Thu, 07 May 2020 14:29:40 +0200

pbcopper (1.4.0+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * ppc64 needs same means like s390 (which is not complete anyway)

  [ Michael R. Crusoe ]
  * Switch to libsimde-dev from our code copy
  * mark the the library and -dev as Multi-Arch: same
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Repository, Repository-Browse, Bug-Database,
    Bug-Submit.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 19 Apr 2020 17:40:27 +0200

pbcopper (1.3.0+dfsg-3) unstable; urgency=medium

  * Remove unused license paragraph
  * Increase timeout for s390 and make sure test will pass in any case

 -- Andreas Tille <tille@debian.org>  Wed, 29 Jan 2020 09:16:01 +0100

pbcopper (1.3.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add the SIMD everywhere headers so we can build on all archs again
    Closes: #947717

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 29 Dec 2019 18:58:05 +0100

pbcopper (1.3.0+dfsg-1) unstable; urgency=medium

  * Fix watch file
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Remove patch git-version.patch that is missing from
    debian/patches/series.
  * Build-Depends: meson, libgtest-dev
  * Meson does not build static lib - leave this out for the moment
  * Split binary packages into dynamic lib and development package

 -- Andreas Tille <tille@debian.org>  Tue, 17 Dec 2019 11:16:46 +0100

pbcopper (0.4.1+dfsg-3) unstable; urgency=medium

  [ Afif Elghraoui ]
  * Remove myself from Uploaders

  [ Andreas Tille ]
  * Skip test relying on clock setting (Thanks for the patch to
    Santiago Vila <sanvila@debian.org>)
    Closes: #927239
  * Add myself to Uploaders
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Wed, 17 Apr 2019 08:21:34 +0200

pbcopper (0.4.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Unanimity is now in unstable
    Closes: #852654
  * Standards-Version: 4.2.1
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Sat, 22 Dec 2018 07:26:24 +0100

pbcopper (0.4.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * d/watch: Upstream is now using release tags
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.0
  * Respect DEB_BUILD_OPTIONS in dh_auto_test

 -- Andreas Tille <tille@debian.org>  Fri, 17 Aug 2018 15:22:14 +0200

pbcopper (0.0.1+20161202-2) unstable; urgency=medium

  * build with -fPIC

 -- Afif Elghraoui <afif@debian.org>  Thu, 22 Dec 2016 00:47:01 -0800

pbcopper (0.0.1+20161202-1) unstable; urgency=medium

  * Initial release (Closes: #848122)

 -- Afif Elghraoui <afif@debian.org>  Sat, 17 Dec 2016 01:18:00 -0800
